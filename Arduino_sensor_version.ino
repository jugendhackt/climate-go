#define LEN 9
#include <Wire.h>
#include <HDC1000.h>

int smokeA0 = A5;
HDC1000 mySensor;
unsigned char incomingByte = 0;
unsigned char buf[LEN];
int PM2_5Val = 0;
int PM10Val = 0;
void setup() {
  Serial.begin(9600);
  Serial.println("start");
  pinMode(smokeA0, INPUT);

}


void loop() {
  int analogSensor = analogRead(smokeA0);
  //   send data only when you receive data;
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    if (incomingByte == 0xAA) {
      Serial.readBytes(buf, LEN);
      PM2_5Val = ((buf[2] << 8) + buf[1]) / 10;
      PM10Val = ((buf[4] << 8) + buf[3]) / 10;
      Serial.print("{\"Feinstaub\": ");
      Serial.print(PM10Val);
      Serial.print(",\"Gas\": ");
      Serial.print(analogSensor);
      Serial.print("}");
      Serial.println();
    }


  }
}

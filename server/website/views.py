from django.shortcuts import render


# Create your views here.
def index(request):
    context = {'title': 'Climate GO Website'}
    return render(request, 'website/index.html', context)

def map(request):
    context = {'title': 'Map - Climate GO'}
    return render(request, 'website/map.html', context)

def agb(request):
    context = {'title': 'Climate GO AGB'}
    return render(request, 'website/agb.html', context)
from django.urls import path, include
from . import views

urlpatterns = [
    path('get/bulk/', views.get_bulk, name='get_bulk'),
    path('get/filter/', views.get_filter, name='get_filter'),
    path('get/', views.get, name='get'),
    path('post/', views.post, name='post'),
    path('', views.api, name='api_index'),
    path('get/types/', views.get_types, name='get_types'),
]

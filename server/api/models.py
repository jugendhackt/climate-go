from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class MeasurementType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class DataPacket(models.Model):
    value = models.IntegerField()
    timestamp = models.DateTimeField()
    longitude = models.FloatField()
    latitude = models.FloatField()
    user_commited = models.ForeignKey(User, models.SET_NULL, null=True)
    type = models.ForeignKey(MeasurementType, models.SET_NULL, null=True)

    def __str__(self):
        return str(self.type) + '::' + str(self.timestamp) + '::' + str(self.value)

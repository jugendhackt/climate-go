from django.shortcuts import render
from django.http import HttpResponseBadRequest, JsonResponse
from .models import DataPacket, MeasurementType
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
import re

from random import randint
from datetime import datetime


def genrandom(n, user, type):
    for i in range(n):
        DataPacket.objects.create(value=randint(0, 500), timestamp=datetime.now(),
                                  longitude=float(str(randint(0, 180)) + '.' + str(randint(0, 2000))),
                                  latitude=float(str(randint(0, 180)) + '.' + str(randint(0, 2000))),
                                  user_commited=user, type=type)


def checkdata(postdata):
    try:
        int(postdata['value'])
        if re.search(r'[a-zA-Z]', postdata['longitude']) is not None:
            return False
        if re.search(r'[a-zA-Z]', postdata['latitude']) is not None:
            return False

        return True
    except ValueError:
        return False


# Create your views here.
def api(request):
    genrandom(10, request.user, MeasurementType.objects.all()[0])  # TODO REMOVE THIS
    context = {'title': 'Climate-go API'}
    return render(request, 'api/api.html', context)


def get_bulk(request):
    if request.method == 'GET':
        data_packets = DataPacket.objects.all()
        data_packets_json = {}

        for n, packet in enumerate(data_packets):
            packet_dict = {'value': int(packet.value), 'timestamp': str(packet.timestamp),
                           'longitude': packet.longitude,
                           'latitude': packet.latitude, 'user': packet.user_commited.username, 'type': str(packet.type)}
            data_packets_json[n] = packet_dict

        # serialized = serialize('json', data_packets_json, safe=False)
        return JsonResponse(data_packets_json)
    else:
        return HttpResponseBadRequest()


def get_filter(request):
    if request.method == 'GET':
        print(request.GET)
        data_packets = DataPacket.objects.filter()
        if "type" in request.GET.keys():
            get_type = request.GET["type"]
            data_packets = data_packets.filter(type__name=get_type)
        if "newer_than" in request.GET.keys():
            get_date = request.GET["newer_than"]
            date_object = datetime.strptime(get_date, "%Y-%m-%d")
            data_packets = data_packets.filter(timestamp__gte=date_object)

        data_packets_json = {}
        for n, packet in enumerate(data_packets):
            packet_dict = {'value': int(packet.value), 'timestamp': str(packet.timestamp),
                           'longitude': packet.longitude,
                           'latitude': packet.latitude, 'user': packet.user_commited.username, 'type': str(packet.type)}
            data_packets_json[n] = packet_dict

        print(data_packets_json)
        # serialized = serialize('json', data_packets_json, safe=False)
        return JsonResponse(data_packets_json)

    else:
        return HttpResponseBadRequest()


def get_types(request):
    if request.method == 'GET':
        measurement_types = MeasurementType.objects.all()
        return JsonResponse(list(measurement_types.values("name")), safe=False)
    else:
        return HttpResponseBadRequest()


# -----------

def get(request):
    if request.method == 'GET':
        context = {'title': 'Climate-go API GET'}
        return render(request, 'api/api.html', context)
    else:
        return HttpResponseBadRequest()


# TODO This is hacky and extremely insecure
@csrf_exempt
def post(request):
    if request.method == 'POST':
        if checkdata(request.POST):
            neededtype = MeasurementType.objects.all().filter(name=request.POST['type'])[0]
            DataPacket.objects.create(value=request.POST['value'], timestamp=datetime.now(),
                                      longitude=request.POST['longitude'],
                                      latitude=request.POST['latitude'], user_commited=User.objects.all()[0],
                                      # TODO Authentification
                                      type=neededtype)
            context = {'title': 'Climate-go API POST'}
            return render(request, 'api/api.html', context)
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseBadRequest()

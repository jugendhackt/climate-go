from django.contrib import admin
from .models import DataPacket, MeasurementType

# Register your models here.
admin.site.register(DataPacket)
admin.site.register(MeasurementType)
